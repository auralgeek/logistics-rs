FROM rust:1.23.0

WORKDIR /app

COPY . /app

RUN cargo install

CMD ["app"]
