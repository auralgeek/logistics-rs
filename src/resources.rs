extern crate tokio;

use std::net::{IpAddr, Ipv4Addr};
use std::sync::{Arc, Mutex, Weak};
use std::{thread, time};

// Tuner
pub struct Tuner {
    pub id: u32,
    pub freq: f64,
}

impl Tuner {
    pub fn new(id: u32) -> Tuner {
        Tuner { id: id, freq: 0.0 }
    }

    pub fn tune(&mut self, freq: f64) {
        self.freq = freq;
        thread::sleep(time::Duration::from_millis(100));
    }

    pub fn capture(&mut self, capture_duration: f64) -> Vec<i16> {
        println!("Capturing {} s...", capture_duration);
        thread::sleep(time::Duration::from_millis(capture_duration as u64 * 1000));
        return Vec::new();
    }
}

// CardState can be unallocated, or looking at some input at some frequency
#[derive(Debug)]
pub struct CardState {
    input: u32,
    freq: f64,
    range: f64,
}

impl CardState {
    pub fn new(input: u32, freq: f64, range: f64) -> CardState {
        CardState {
            input: input,
            freq: freq,
            range: range,
        }
    }

    pub fn covers(&self, input: u32, freq: f64) -> bool {
        if self.input == input
            && self.freq - self.range / 2.0 < freq
            && self.freq + self.range / 2.0 > freq
        {
            return true;
        } else {
            return false;
        }
    }
}

// Card
pub struct Card {
    pub addr: IpAddr,
    pub tuners: Arc<Vec<Mutex<Tuner>>>,
    pub state: Weak<CardState>,
}

impl Card {
    pub fn new(addr: IpAddr, num_tuners: u32) -> Card {
        let mut tuners = Vec::new();
        for i in 0..num_tuners {
            tuners.push(Mutex::new(Tuner::new(i)));
        }

        Card {
            addr: addr,
            tuners: Arc::new(tuners),
            state: Weak::new(),
        }
    }

    pub fn tune(&self) {
        if self.state.upgrade().is_some() {
            let state = self.state.upgrade().unwrap();
            println!("Setting switch input to {}", state.input);
            println!("Setting center frequency to {}", state.freq);
            thread::sleep(time::Duration::from_millis(3000));
        } else {
            println!("Can't tune card with no CardState!");
        }
    }
}

pub struct CardManager {
    pub cards: Vec<Card>,
}

impl CardManager {
    pub fn new(num_cards: u32, num_tuners: u32) -> CardManager {
        let mut cards = Vec::new();
        for i in 0..num_cards {
            cards.push(Card::new(
                IpAddr::V4(Ipv4Addr::new(127, 0, i as u8, 0)),
                num_tuners,
            ));
        }
        CardManager { cards: cards }
    }

    // This should take an input job, and allocate the card, and toss the Tuner
    // work into an async pool of workers
    fn execute_job(&mut self, input: u32, freq: f64) -> thread::JoinHandle<()> {
        println!("Executing new job: ({}, {})", input, freq);

        loop {
            // First see if something is tuned there already...
            for card in &self.cards {
                if card.state.upgrade().is_some() {
                    let strong_state = card.state.upgrade().unwrap();
                    if strong_state.covers(input, freq) {
                        println!("Found a card tuned to our spectrum of interest!");

                        // Clone a new Arc to state, pass along chain
                        let new_state = Arc::clone(&strong_state);

                        // Allocate Tuner, pass lock ownership to worker
                        let tuners = Arc::clone(&card.tuners);

                        return thread::spawn(move || {
                            loop {
                                for tuner in tuners.iter() {
                                    let mut lock = tuner.try_lock();
                                    if let Ok(ref mut tuner) = lock {
                                        // Tune the Tuner
                                        tuner.tune(freq - new_state.freq);

                                        // Perform capture
                                        tuner.capture(1.0);

                                        // TODO: Queue result

                                        return;
                                    }
                                }
                            }

                            // If all Tuners busy, we sleep for a while and try
                            // again to see if some task has freed up a Tuner
                            thread::sleep(time::Duration::from_millis(10));
                        });
                    }
                }
            }

            // Now check if anything is unallocated...
            for mut card in &mut self.cards {
                if card.state.upgrade().is_none() {
                    println!("Found unallocated card!");

                    // Allocate new CardState, set Card Weak<CardState>, tune Card
                    let new_state = Arc::new(CardState::new(input, freq, 100.0));
                    card.state = Arc::downgrade(&new_state);
                    card.tune();

                    // Create a new worker thread who uses the new_state, and has a
                    // reference to card.tuners
                    let tuners = Arc::clone(&card.tuners);

                    return thread::spawn(move || {
                        loop {
                            for tuner in tuners.iter() {
                                let mut lock = tuner.try_lock();
                                if let Ok(ref mut tuner) = lock {
                                    // Tune the Tuner
                                    tuner.tune(freq - new_state.freq);

                                    // Perform capture
                                    tuner.capture(4.0);

                                    // TODO: Queue result

                                    return;
                                }
                            }
                        }

                        // If all Tuners busy, we sleep for a while and try
                        // again to see if some task has freed up a Tuner
                        thread::sleep(time::Duration::from_millis(10));
                    });
                }
            }

            // If neither, we sleep for a while and try again to see if some task
            // has freed up resources
            thread::sleep(time::Duration::from_millis(10));
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::distributions::{Distribution, Uniform};
    use rand::Rng;

    #[test]
    fn tuner_new() {
        let tuner = Tuner::new(2);
        assert_eq!(2, tuner.id);
    }

    #[test]
    fn tuner_tune() {
        let mut tuner = Tuner::new(2);
        tuner.tune(100.0);
        assert_eq!(100.0, tuner.freq);
    }

    #[test]
    fn tuner_capture() {
        let mut tuner = Tuner::new(2);
        let capture = tuner.capture(4.0);
        assert_eq!(capture, Vec::new());
    }

    #[test]
    fn card_new() {
        let ipaddr = IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1));
        let card = Card::new(ipaddr, 4);
        assert_eq!(card.addr, ipaddr);
        assert_eq!(card.tuners.len(), 4);
    }

    #[test]
    fn card_manager_execute_job() {
        let mut manager = CardManager::new(2, 2);
        let mut handles = Vec::new();
        for i in 0..10 {
            handles.push(manager.execute_job(i, 100.0));
        }
        for handle in handles {
            handle.join().unwrap();
        }
        assert_eq!(2, 2);
    }

    #[test]
    fn card_manager_execute_job2() {
        let mut manager = CardManager::new(8, 4);
        let mut handles = Vec::new();
        let between = Uniform::from(0..12);
        let mut rng = rand::thread_rng();
        for i in 0..10 {
            println!("Adding job {}", i);
            let input: u32 = between.sample(&mut rng);
            let mut freq: f64 = rng.gen();
            freq *= 1000.0;
            freq += 1000.0;
            handles.push(manager.execute_job(input, freq));
        }
        for handle in handles {
            handle.join().unwrap();
        }
        assert_eq!(2, 2);
    }
}
